﻿using UnityEngine;
using System.Collections;
using Facebook.Unity;
using System;
using System.Collections.Generic;
using Facebook.MiniJSON;

public class FacebookManager : MonoBehaviour
{
    public static FacebookManager Instance;

    void Awake()
    {
        Instance = this;
        if (!FB.IsInitialized)
        {
            // Initialize the Facebook SDK
            FB.Init(InitCallback, OnHideUnity);
        }
        else
        {
            // Already initialized, signal an app activation App Event
            FB.ActivateApp();
        }
    }

    private void InitCallback()
    {
        if (FB.IsInitialized)
        {
            // Signal an app activation App Event
            FB.ActivateApp();
        }
        else
        {
            Debug.Log("Failed to Initialize the Facebook SDK");
        }
    }

    #region Login / Logout
    public void LoginWithReadPermission()
    {
        var perms = new List<string>() { "public_profile", "email", "user_friends" };
        FB.LogInWithReadPermissions(perms, AuthCallback);
    }

    public void LoginWithPublishPermission()
    {
        var perms = new List<string>() { "publish_actions" };
        FB.LogInWithPublishPermissions(perms, AuthCallback);
    }

    public void FacebookLogout()
    {
        FB.LogOut();
    }

    private void AuthCallback(ILoginResult result)
    {
        if (FB.IsLoggedIn)
        {
            // AccessToken class will have session details
            var aToken = Facebook.Unity.AccessToken.CurrentAccessToken;
            // Print current access token's User ID
            Debug.Log(aToken.UserId);
            // Print current access token's granted permissions
            foreach (string perm in aToken.Permissions)
            {
                Debug.Log(perm);
            }
        }
        else
        {
            Debug.Log("User cancelled login");
        }
    }
    #endregion

    #region Sharing
    public void FacebookShare()
    {
        FB.ShareLink(new System.Uri("http://resocoder.com"), "Check it out!",
            "Good programming tutorials lol!",
            new System.Uri("http://resocoder.com/wp-content/uploads/2017/01/logoRound512.png"),
            ShareCallback
            );
    }

    //test id 100017120267181
    private void ShareCallback(IShareResult result)
    {
        if (result.Cancelled || !String.IsNullOrEmpty(result.Error))
        {
            Debug.Log("ShareLink Error: " + result.Error);
        }
        else if (!String.IsNullOrEmpty(result.PostId))
        {
            // Print post identifier of the shared content
            Debug.Log(result.PostId);
        }
        else
        {
            // Share succeeded without postID
            Debug.Log("ShareLink success!");
        }
    }
    #endregion

    #region Inviting

    public void FacebookGameRequest()
    {
        FB.AppRequest("Hey! Come and play this awesome game!", title: "Reso Coder Tutorial");
    }

    public void FacebookInvite()
    {
        FB.Mobile.AppInvite(
            new Uri("https://fb.me/1781533425470411"),
            new Uri("https://scontent-waw1-1.xx.fbcdn.net/v/t31.0-8/21272845_512355599115972_5892370551115834241_o.jpg?oh=41f7b52faf35b779828b02b66bbf11f6&oe=5A1FCA6E"),
            AppInviteCallback
            );
    }

    private void AppInviteCallback(IAppInviteResult result)
    {
        if (result.Cancelled || !String.IsNullOrEmpty(result.Error))
        {
            Debug.Log("InviteLink Error: " + result.Error);
        }
        else
        {
            Debug.Log("InviteLink success!");
        }
    }
    #endregion

    #region Screenshooting
    public void PostScreenshot()
    {
        StartCoroutine("TakeScreenshot");
    }

    private IEnumerator TakeScreenshot()
    {
        yield return new WaitForEndOfFrame();

        var width = Screen.width;
        var height = Screen.height;
        var tex = new Texture2D(width, height, TextureFormat.RGB24, false);
        // Read screen contents into the texture
        tex.ReadPixels(new Rect(0, 0, width, height), 0, 0);
        tex.Apply();
        byte[] screenshot = tex.EncodeToPNG();

        var wwwForm = new WWWForm();
        wwwForm.AddBinaryData("image", screenshot, "Screenshot.png");

        FB.API("me/photos", HttpMethod.POST, APICallback, wwwForm);
    }

    private void APICallback(IGraphResult result)
    {
        if (result.Cancelled || !String.IsNullOrEmpty(result.Error))
        {
            Debug.Log("Screenshot post Error: " + result.Error);
        }
        else
        {
            Debug.Log("Screenshot post success!");
        }
    }
    #endregion

    public void GetFriendsPlayingThisGame()
    {
        string query = "/me/friends";
        FB.API(query, HttpMethod.GET, result =>
        {
            var dictionary = (Dictionary<string, object>)Json.Deserialize(result.RawResult);
            var friendsList = (List<object>)dictionary["data"];
        });
    }

    private void OnHideUnity(bool isGameShown)
    {
        if (!isGameShown)
        {
            // Pause the game - we will need to hide
            Time.timeScale = 0;
        }
        else
        {
            // Resume the game - we're getting focus again
            Time.timeScale = 1;
        }
    }
}